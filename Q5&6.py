"""
Test Code for ques 5 & 6
"""

import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import numpy as np
from netCDF4 import Dataset
import datetime
from datetime import date
from datetime import timedelta
import damop_model as dm

def read_era(filename, iprint):

    '''
    Read in the ground variable data from the netCDF file.
    Input: name of file to read.
    Output: 
    :longitude  - degrees
    :latitude   - degrees
    :time       - month number
    :runoff     - runoff (m)
    '''
    data = Dataset(filename, 'r')
    if iprint == 1:
        print(data)
        print()
        print(data.dimensions)
        print()
        print(data.variables)
        print()
        
    rtime = data.variables['time'][:]
    alon = data.variables['longitude'][:]
    alat = data.variables['latitude'][:]
    runoff = data.variables['ro'][:,:,:]  
    data.close()
    #
    # Time is in hours since 00UT, 1 Jan 1900.
    # Convert to timedelta format.
    #
    ftime = float(rtime)
    dtime = timedelta(hours=ftime)
    #
    # Note that you can add times in datetime and timedelta formats
    # which allows for leap years etc in time calculations.
    #
    startcal = datetime.datetime(1900, 1, 1)
    newcal = startcal+dtime
    print('Read data for accumulation to ',newcal)

    return alon, alat, newcal, runoff



def read_s2s(filename_s2s, iprint):
    '''
    Read in the S2S data from the netCDF file.
    Input: name of file to read.
    Output: 
    :longitude  - degrees
    :latitude   - degrees
    :time       - month number
    :runoff     - runoff (m)
    '''
    data = Dataset(filename_s2s, 'r')
    if iprint == 1:
        print(data)
        print()
        print(data.dimensions)
        print()
        print(data.variables)
        print()
        
    rtime = data.variables['time'][:]
    number = data.variables['number'][:]        # Ensemble members
    alat = data.variables['latitude'][:]
    alon = data.variables['longitude'][:]
    #s2s_runoff = data.variables['ro'][:,0,:,:]  
    s2s_runoff = data.variables['ro'][:,0,:,:]
    #s2s_runoff = s2s_runoff/1000
    data.close()
    #
    # Time is in hours since 00UT, 1 Jan 1900.
    # Convert to timedelta format.
    #
    dtime = datetime.timedelta(hours=1)*rtime
    #
    # Note that you can add times in datetime and timedelta formats
    # which allows for leap years etc in time calculations.
    #
    startcal = datetime.datetime(1900, 1, 1)
    newcal = startcal+dtime
    print('Read data for accumulation to ',newcal[0])

    return alon, alat, newcal, s2s_runoff
    

def subset_field(alon, alat, lonpick, latpick):

    '''
    Find the indices of the grid point centred closest to chosen location.
    Input: 
    :alon       - longitude points
    :alat       - latitude points
    :lonpick    - longitude of chosen location
    :latpick    - latitude of chosen location
    Output:
    :intlon     - index of longitude for chosen point
    :intlat     = index of latitude for chosen point
    '''
    #
    # Using the fact that longitude and latitude are regularly spaced on grid.
    # Also, points ordered from north to south in latitude.
    # The indices (intlon, intlat) correspond to the centre of the closest grid-box 
    # to the chosen location.
    #
    dlon = alon[1]-alon[0]
    dlat = alat[1]-alat[0] # note that dlat is negative due to ordering of grid
    lonwest = alon[0]-0.5*dlon
    latnorth = alat[0]-0.5*dlat
    intlon = int(round((lonpick-lonwest)/dlon))
    intlat = int(round((latpick-latnorth)/dlat))
    print()
    print('Longitude of nearest grid box = ',alon[intlon])
    print('Latitude of nearest grid box = ',alat[intlat])
    
    return intlon, intlat


def plot_basic(alon,alat,itime,field3d,fieldname):

    """
    Plot 2-D field as a simple pixel image.
    Input: longitude, latitude, time-index, infield, name of field
    Output: Plot of field"""
    
    field = field3d[itime,:,:]
    fig = plt.figure()
    plt.imshow(field,interpolation='nearest')
    plt.colorbar(pad=0.04,fraction=0.046)
    plt.title(fieldname)
    plt.show()

    return

    
def plot_onproj(alon,alat,itime,field3d,fieldname):

    '''
    Plot 2-D field on map using cartopy map projection.
    Input: longitude, latitude, time-index, infield, name of field
    Output: Plot of field
    '''  
    field = field3d[itime,:,:]
    fig = plt.figure()
    ax = plt.axes(projection=ccrs.PlateCarree())
    ax.coastlines(resolution='50m', color='black', linewidth=1)
    ax.gridlines()
    plt.title(fieldname)
    nlevs = 20
    plt.contourf(alon, alat, field, nlevs,
             transform=ccrs.PlateCarree())
    plt.show()

    return


def plot_series(timarr, y, ylabel, mytitle):
    '''
    Plot the subset time series
    Inputs:
        timarr   - time array in datetime format
        y        - data time series
        ylabel   - string name for data
        mytitle  - plot title
    '''
    fig = plt.figure()
    plt.plot(timarr,y,label=ylabel)
    plt.xlabel("Days")
    plt.ylabel(ylabel)
    plt.title(mytitle)
    plt.legend()
    plt.show()


def plot_series2(timarr, inflow, x, w, r, powergen, ilabel, xlabel, wlabel, rlabel, pwrlabel, mytitle):
    fig = plt.figure()
    plt.plot(timarr,inflow,label=ilabel)
    plt.plot(timarr, x, label=xlabel)
    plt.plot(timarr, w, label=wlabel)
    plt.plot(timarr, r, label=rlabel)
    plt.plot(timarr, powergen, label=pwrlabel)
    plt.xlabel("Days")
    #plt.ylabel(ylabel)
    plt.title(mytitle)
    plt.legend()
    plt.show()
    
    
    
def extract_series(fpath, fstem, lonpick, latpick, dstart, dend):
    '''
    High level function controlling extraction of runoff time series 
    for chosen location.
    Input: fpath, fstem determine the name of file to read
    :lonpick    - longitude of chosen location
    :latpick    - latitude of chosen location
    :dstart     - start date in datetime.date format
    :dend       - end date in datetime.date format
    Output: 
    :dayarr     - time in days since start
    :timarr     - time series in datetime format
    :runoffloc  - runoff (m) time series at chosen location
    '''   
    #
    # Set end date and start date of required time series
    #
    dendp = dend+timedelta(days=1)
    tinterval = dendp-dstart
    ndays = tinterval.days
    #
    # Plot the data for the first date in the interval
    #
    fdate = dstart.strftime("%Y%m%d")
    iprint = 0  # set to 1 to print variables on reading files; 0 for no print
    #Read the data
    filename = str(fpath+fstem+fdate+'.nc')
    #filename_s2s = str(fpath_s2s+fstem_s2s+fdate+'.nc')
    # Note that the str() function is included to ensure that these
    # variables are interpreted as character strings.
    alon, alat, time, runoff = read_era(filename, iprint)
    #alon, alat, newcal, s2s_runoff = read_s2s(filename_s2s, iprint)
    #
    # Find the indices of the grid box centred closest to the chosen location
    #
    intlon, intlat = subset_field(alon, alat, lonpick, latpick)
    #
    # Plot runoff on a map at time point itime
    #
    itime = 0
    plot_basic(alon,alat,itime,runoff,'runoff  (m)')
    #plot_basic(alon,alat,itime,s2s_runoff,'S2S runoff  (m)')
    #plot_onproj(alon,alat,itime,runoff,'runoff  (m)')
    #
    # Setup arrays to save time series data
    #
    dayarr = np.arange(ndays)
    timarr = np.arange(np.datetime64(str(dstart)), np.datetime64(str(dendp)))
    runoffarr = np.zeros(ndays)
    #s2s_runoffarr = np.zeros(ndays)
    #
    # Loop over dates, reading files and saving data
    #
    dcur = dstart
    for n in range(ndays):
        fdate = dcur.strftime("%Y%m%d")
        #Read the data
        filename = str(fpath+fstem+fdate+'.nc')
        #filename_s2s = str(fpath_s2s+fstem_s2s+fdate+'.nc')
        
        # Note that the str() function is included to ensure that these
        # variables are interpreted as character strings.
        alon, alat, time, runoff = read_era(filename, iprint)
        #alon, alat, newcal, s2s_runoff = read_s2s(filename_s2s, iprint)

        # Save the data required from this time-point

        runoffarr[n] = runoff[0, intlat, intlon]
        #s2s_runoffarr[n] = s2s_runoff[0, intlat, intlon]
        #
        # Increment the date variable by one day
        #
        dcur=dcur+timedelta(days=1)
    #
    # Now plot the time series
    #
    varname = 'runoff  (m)'
    mytitle = 'Runoff time series for chosen location'
    plot_series(dayarr, runoffarr, varname, mytitle)
    
    return dayarr, timarr, runoffarr

def S2S_one_forecast(fname, lonpick, latpick, dstart, dend):
    
    #
    # Set end date and start date of required time series
    #
    dendp = dend+timedelta(days=1)
    tinterval = dendp-dstart
    ndays = tinterval.days
    
    iprint = 0# set to 1 to print variables on reading files; 0 for no print
    #Read the data
    #fname = 'C:/Users/DELL/Desktop/UoR/ClimateServices/Project/data/japan_LFPW_2018/japan_LFPW_PF.20180705.nc'
    file_one = fname
    alon, alat, newcal, s2s_runoff = read_s2s(file_one, iprint)
    print('newcal',newcal)
    print(newcal.shape)
    #
    # Find the indices of the grid box centred closest to the chosen location
    #
    intlon, intlat = subset_field(alon, alat, lonpick, latpick)
    # Plot runoff on a map at time point itime
    #
    itime = 0
    plot_basic(alon,alat,itime,s2s_runoff,'S2S runoff  (m) Ensemble:1')
    
    ndays2 = len(newcal)
    dayarr2 = np.arange(ndays2)
    timarr2 = np.arange(np.datetime64(str(dstart)), np.datetime64(str(dendp)))
    s2s_runoffarr = np.zeros(ndays2)


    for n in range(ndays2):
        #Read the data
        file_one = fname
        
        # Note that the str() function is included to ensure that these
        # variables are interpreted as character strings.
        alon, alat, newcal, s2s_runoff = read_s2s(file_one, iprint)
        # Save the data required from this time-point
        
        s2s_runoffarr[n] = s2s_runoff[n, intlat, intlon]


    varname2 = 'S2S runoff (m)'
    mytitle2 = 'S2S Runoff time series for chosen location'
    plot_series(dayarr2, s2s_runoffarr, varname2, mytitle2)
    
    return dayarr2, timarr2, s2s_runoffarr

def join_array(runoffarr, s2s_runoffarr, dayarr, dayarr2):
    era_s2s_arr = np.hstack((runoffarr, s2s_runoffarr))
    dayarr_join = np.hstack((dayarr, dayarr2))
    
    return dayarr_join, era_s2s_arr


if __name__ == '__main__':
    
    '''
    Main program script extracting time series from ERA5 data.
    '''
    # Set up the parameters required for the dam operation model
    h_dam = 161     # m
    res_area = 13*1e+6      # m^2
    kappa = res_area/2      # m^2
    catcharea = 254*1e+6    # m^2
    tau = 180               # Timescale for dam to run dry at maximum flow rate (days)
    tau = tau*24*3600       # in seconds    
    sigma = 0.9
    gmax = 153              # Maximum Power (MW)
    
    
    
    # Constraints on dam height to optimise for maximum power generation
    hmax = 0.5*h_dam        # (m) Maximum safe head of water at dam 
    hmin = 0.2*hmax         # (m) Minimum allowed head of water at dam
    wmax = (kappa/tau)*h_dam
    rmax = 0.2*wmax         # Maximum relief flow
    mu = gmax/(sigma*wmax*h_dam)    #
    beta = 0.1              # (minimum)
    wmin = beta*wmax
#
    # Pick the location to extract the time series
    #
    lonpick = 136.47642
    latpick = 35.63422
    # ?? Is this related to Q3 Averaging of grid points?
    dlon = 0.1              # runoff data grid box side in degree longitude
    dlat = 0.1 
    r_earth = 6371000       # Average radius of Earth (m)
    boxarea = dlon*(np.pi/180)*dlat*(np.pi/180)*np.cos(latpick*np.pi/180)*r_earth**2
    #
    # Select the start and end date required for the time series
    #
    dstart = datetime.date(2018, 5, 26)
    dend = datetime.date(2018, 7, 25)
    fname = 'C:/Users/DELL/Desktop/UoR/ClimateServices/Project/data/japan_LFPW_2018/japan_LFPW_PF.20180726.nc'
    iprint=0
    alon, alat, newcal, s2s_runoff = read_s2s(fname, iprint)
    print('read_s2s output')
    print(alon, alat, newcal, s2s_runoff, s2s_runoff.shape)
    dayarr2, timarr2, s2s_runoffarr = S2S_one_forecast(fname, lonpick, latpick, dstart, dend)

    # Set the path and filename stem for data files.   
    #
    fpath = 'C:/Users/DELL/Desktop/UoR/ClimateServices/Project/data/'
    fstem = 'japan_ERA5land.'


    dayarr, timarr, runoffarr = extract_series(fpath, fstem, lonpick, latpick, dstart, dend)
    dt = dayarr[1] - dayarr[0]          # days in the interval 
    dt = dt*24*3600                     # Interval time in seconds (s)
    n = len(dayarr)
    
    # ?? Q4 Can we not simply input s2s_runoffarr in dam model without re-calibrating?
    
    # Call the dam operation model to obatin the results for optimum power 
    # generated when relevant constarints are applied to the reservoir level
    # and the flow through turbines
    inflow, x, w, r, gout = dm.damop_model(runoffarr, dt, catcharea, kappa, hmax, hmin, wmax, wmin, rmax, sigma)
    print('runoffarr',runoffarr.shape)
    
    
    powergen = mu*sigma*w*x         # Power generation rate

    # Plot the outputs of the dam operation model using plot_series function
    # defined above
    #
    # Define the required labels for each variable
    ilabel = 'ERA Inflow (m^3 s^-1)'
    xlabel = ' Head (m)'
    wlabel = 'Flow rate (m^3 s^-1)'
    rlabel = 'Reflief flow (m^3 s^-1)'
    pwrlabel = 'Generation rate (MW)'
    mytitle = 'Dam Model Optimization Output'
    #
    plot_series2(dayarr, inflow, x, w, r, powergen, ilabel, xlabel, wlabel, rlabel, pwrlabel, mytitle)
    
    dayarr2, timarr2, s2s_runoffarr = S2S_one_forecast(fname, lonpick, latpick, dstart, dend)
    
    # Combine the ERA and S2S to get a single array
    dayarr_join, era_s2s_arr = join_array(runoffarr, s2s_runoffarr, dayarr, dayarr2)
    
    plt.plot(dayarr_join, era_s2s_arr, label='Combined Runoff')
    #plt.plot(dayarr_join, runoffarr, label='ERA Runoff')
    #plt.plot(dayarr_join, s2s_runoffarr, label='S2S Runoff')
    plt.legend
    plt.ylabel('Runoff')
    plt.xlabel('Days')
    plt.title('Combined Runoff of ERA and S2S for 93 days')
    plt.show()
    
    
    dt2 = dayarr_join[1] - dayarr_join[0]
    dt2 = dt2*24*3600
    n2 = len(dayarr_join)
    
    inflow, x2, w2, r2, gout = dm.damop_model(era_s2s_arr, dt2, catcharea, kappa, hmax, hmin, wmax, wmin, rmax, sigma)
    
    print('s2s_runoffarr', s2s_runoffarr.shape)
    print('era_s2s_arr', era_s2s_arr.shape)
    powergen2 = mu*sigma*w2*x2 
    ilabel2 = 'Inflow'
    xlabel = ' Head (m)'
    wlabel = 'Flow rate (m^3 s^-1)'
    rlabel = 'Reflief flow (m^3 s^-1)'
    pwrlabel = 'Generation rate (MW)'
    mytitle = 'Dam Model Optimization Output'
    plot_series2(dayarr_join, inflow, x2, w2, r2, powergen2, ilabel2, xlabel, wlabel, rlabel, pwrlabel, mytitle)
    print('dayarr_join', dayarr_join.shape)

    